export default defineAppConfig({
  pages: [
    'pages/index/index',
    'pages/aiq/index',
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  },
  tabBar: {
    list:[
      {
        pagePath:'pages/index/index',
        text:'首页'
      },
      {
        pagePath:'pages/aiq/index',
        text:'模型'
      }
    ]
  }
})
