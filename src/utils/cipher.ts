import CryptoJs from 'crypto-js';


export function hmac256(text: string, key: string) {
    return CryptoJs.HmacSHA256(text, key)
}

export function encryptByBase64(text: string) {
    return CryptoJs.enc.Base64.stringify(text);
}

export function base64Encode(text: string) {
    var srcs = CryptoJs.enc.Utf8.parse(text);
    var encodeData = CryptoJs.enc.Base64.stringify(srcs);
    return encodeData
}


export function base64Decode(text: string) {
    var srcs = CryptoJs.enc.Base64.parse(text);
    var decodeData = srcs.toString(CryptoJs.enc.Utf8);
    return decodeData
  }